package v1.user

import javax.inject.{Inject, Singleton}

import scala.concurrent.Future

/**
  * Created by stetsa on 2/28/2017.
  */

final case class UserData( var id: UserId, firstName: String, lastName: String, age: Int )

class UserId private (val underlying: Int) extends AnyVal {
  override def toString: String = underlying.toString
}

object UserId {
  def apply(raw: String): UserId = {
    require(raw != null)
    new UserId(Integer.parseInt(raw))
  }
}

trait UserRepository {
  def create(data: UserData): Future[UserId]

  def list(): Future[Iterable[UserData]]

  def delete(id : UserId): Future[Iterable[UserData]]

  def get(id: UserId): Future[Option[UserData]]

  def update(data: UserData): Future[UserId]
}

/**
  * A trivial implementation for the UserRepository.
  */
@Singleton
class UserRepositoryImpl @Inject() extends UserRepository {

  private val logger = org.slf4j.LoggerFactory.getLogger(this.getClass)

  private var userList = List(
    UserData(UserId("1"), "Andrew", "Stetsenko", 27),
    UserData(UserId("2"), "John", "Rock", 13),
    UserData(UserId("3"), "Smoky", "Short", 43),
    UserData(UserId("4"), "Old", "Jimmy", 3),
    UserData(UserId("5"), "Crazy", "Bernard", 74)
  )

  override def list(): Future[Iterable[UserData]] = {
    Future.successful {
      logger.trace(s"list: ")
      userList
    }
  }

  override def get(id: UserId): Future[Option[UserData]] = {
    Future.successful {
      logger.trace(s"get: id = $id")
      userList.find(user => user.id == id)
    }
  }

  def create(data: UserData): Future[UserId] = {
    Future.successful {
      data.id = getNextIndex()
      logger.trace(s"create: data = $data")
      userList = data :: userList
      data.id
    }
  }

  override def delete(id: UserId): Future[Iterable[UserData]] = {
    Future.successful {
      logger.trace(s"list: ")
      userList = userList.filter(!_.id.toString.equals(id.toString))
      userList
    }
  }

  override def update(data: UserData): Future[UserId] = {
    Future.successful {
      logger.trace(s"update user: $data")
      userList = userList.filter(!_.id.toString().equals(data.id.toString))
      userList = data :: userList
      data.id
    }
  }

  def getNextIndex(): UserId = {
    val newId = userList.maxBy(_.id.underlying).id.underlying+1
    UserId(newId.toString)
  }
}

