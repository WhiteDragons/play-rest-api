package v1

import play.api.i18n.Messages

/**
  * Package object for post.  This is a good place to put implicit conversions.
  */
package object user {

  /**
    * Converts between UserRequest and Messages automatically.
    */
  implicit def requestToMessages[A](implicit r: UserRequest[A]): Messages = {
    r.messages
  }
}
