package v1.user

import javax.inject.{Inject, Provider}

import play.api.libs.json.{JsValue, Json, Writes}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by stetsa on 2/28/2017.
  */

  case class UserResource(id: String, link: String, firstName: String, lastName: String, age: Int)

  object UserResource {

    /**
      * Mapping to write a UserResource out as a JSON value.
      */
    implicit val implicitWrites = new Writes[UserResource] {
      def writes(user: UserResource): JsValue = {
        Json.obj(
          "id" -> user.id,
          "link" -> user.link,
          "firstName" -> user.firstName,
          "lastName" -> user.lastName,
          "age" -> user.age
        )
      }
    }
  }

  /**
    * Controls access to the backend data, returning [[UserResource]]
    */
  class UserResourceHandler @Inject()(
                                       routerProvider: Provider[UserRouter],
                                       userRepository: UserRepository)(implicit ec: ExecutionContext) {

    def create(userInput: UserFormInput): Future[UserResource] = {
      val data = UserData(UserId("0"), userInput.firstName, userInput.lastName, userInput.age)
      userRepository.create(data).map { id =>
        createUserResource(data)
      }
    }

    def update(userInput: UserUpdateFormInput): Future[UserResource] = {
      val data = UserData(UserId(userInput.id.toString), userInput.firstName, userInput.lastName, userInput.age)
      userRepository.update(data).map { id =>
        createUserResource(data)
      }
    }

    def lookup(id: String): Future[Option[UserResource]] = {
      val userFuture = userRepository.get(UserId(id))
      userFuture.map { maybeUserData =>
        maybeUserData.map { userData =>
          createUserResource(userData)
        }
      }
    }

    def find: Future[Iterable[UserResource]] = {
      userRepository.list().map { userDataList =>
        userDataList.map(usreData => createUserResource(usreData))
      }
    }

    def delete(id: String): Future[Iterable[UserResource]] = {
      userRepository.delete(UserId(id)).map { userDataList =>
        userDataList.map(userData => createUserResource(userData))
      }
    }

    private def createUserResource(u: UserData): UserResource = {
      UserResource(u.id.toString, routerProvider.get.link(u.id), u.firstName, u.lastName, u.age)
    }
  }

