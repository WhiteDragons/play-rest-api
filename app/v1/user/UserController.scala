package v1.user

import javax.inject.Inject

import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class UserFormInput(firstName: String, lastName: String, age: Int)

case class UserUpdateFormInput(id: Int, firstName: String, lastName: String, age: Int)

/**
  * Takes HTTP requests and produces JSON.
  */
class UserController @Inject()(
    action: UserAction,
    handler: UserResourceHandler)(implicit ec: ExecutionContext)
    extends Controller {

  private val form: Form[UserFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "firstName" -> nonEmptyText,
        "lastName" -> nonEmptyText,
        "age" -> number
      )(UserFormInput.apply)(UserFormInput.unapply)
    )
  }

  private val updateForm: Form[UserUpdateFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "id" -> number,
        "firstName" -> nonEmptyText,
        "lastName" -> nonEmptyText,
        "age" -> number
      )(UserUpdateFormInput.apply)(UserUpdateFormInput.unapply)
    )
  }

  def index: Action[AnyContent] = {
    action.async { implicit request =>
      handler.find.map { users =>
        Ok(Json.toJson(users))
      }
    }
  }

  def process: Action[AnyContent] = {
    action.async { implicit request =>
      processJsonUser()
    }
  }

  def update: Action[AnyContent] = {
    action.async { implicit request =>
      updateJsonUser()
    }
  }

  def show(id: String): Action[AnyContent] = {
    action.async { implicit request =>
      handler.lookup(id).map { user =>
        Ok(Json.toJson(user))
      }
    }
  }

  def delete(id: String): Action[AnyContent] = {
    action.async { implicit request =>
      handler.delete(id).map { users =>
        Ok(Json.toJson(users))
      }
    }
  }

  private def processJsonUser[A]()(
      implicit request: UserRequest[A]): Future[Result] = {
    def failure(badForm: Form[UserFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: UserFormInput) = {
      handler.create(input).map { user =>
        Created(Json.toJson(user)).withHeaders(LOCATION -> user.link)
      }
    }

    form.bindFromRequest().fold(failure, success)
  }

  private def updateJsonUser[A]()(
    implicit request: UserRequest[A]): Future[Result] = {
    def failure(badForm: Form[UserUpdateFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: UserUpdateFormInput) = {
      handler.update(input).map { user =>
        Created(Json.toJson(user)).withHeaders(LOCATION -> user.link)
      }
    }

    updateForm.bindFromRequest().fold(failure, success)
  }
}
