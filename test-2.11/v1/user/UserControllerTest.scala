package v1.user


import java.io.File

import org.scalatestplus.play.{OneServerPerSuite, PlaySpec}
import play.api.inject.guice.{GuiceApplicationBuilder, GuiceInjectorBuilder}
import play.api.libs.ws.WSClient
import play.api.mvc.{Action, Result}
import play.api.routing.Router
import javax.inject._

import com.google.inject.AbstractModule
import v1.user._
import org.scalatest._
import org.scalatestplus.play._
import play.api.Mode
import play.i18n.{Lang, Messages, MessagesApi}
import play.api.test.Helpers.{await, GET => GET_REQUEST, _}
import play.api.mvc._
import play.api.routing.sird._
import play.api.inject.bind

/**
  * Created by stetsa on 3/1/2017.
  */

class UserControllerTest extends PlaySpec with OneServerPerSuite with Results {

  val injector = new GuiceInjectorBuilder()
    .overrides(bind[UserRepository].to[UserRepositoryImpl])
    .injector

  val repo = injector.instanceOf[UserRepositoryImpl]

  "User repository " should {

    "should return list of values on list()" in {
      val list = await(repo.list())
      list.size mustBe 5
    }

    "should be able to run Add one user to list" in {
      val newbe = await(repo.create(new UserData(UserId("6"), "ME", "Test", 3)))
      newbe.toString mustBe "6"
    }

    "should be able to Update one user in a list" in {
      val updatePayload = new UserData(UserId("6"), "AFTER", "UPDATE", 3)
      val id = await(repo.update(updatePayload))
      val updated = await(repo.get(id))
      updated.get mustBe updatePayload
    }

    "should be able to Delete one user from a list" in {
      val list = await(repo.delete(UserId("6")))
      list.size mustBe 5
    }

    "should return empty list when last element deleted" in {
      await(repo.delete(UserId("1")))
      await(repo.delete(UserId("2")))
      await(repo.delete(UserId("3")))
      await(repo.delete(UserId("4")))
      await(repo.delete(UserId("5")))
      val list = await(repo.delete(UserId("6")))
      list.size mustBe 0
    }

    "should create new user in case update operation haven't found user by id" in {
      val updatePayload = new UserData(UserId("123"), "AFTER", "UPDATE", 3)
      val id = await(repo.update(updatePayload))
      val updated = await(repo.get(id))
      updated.get mustBe updatePayload
    }
  }

}
