
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/stetsa/Projects/rest-api/conf/routes
// @DATE:Wed Mar 01 19:54:15 CST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
