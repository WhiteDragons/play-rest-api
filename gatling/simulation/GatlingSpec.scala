package simulation

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.language.postfixOps

class GatlingSpec extends Simulation {

  val httpConf = http.baseURL("http://localhost:9000/v1/users")

  val readClients = scenario("Clients").exec(Index.refreshManyTimes)

  setUp(
    readClients.inject(rampUsers(100) over (20 seconds)).protocols(httpConf)
  )
}

object Index {

  def refreshAfterOneSecond =
    exec(http("Index").get("/").check(status.is(200))).pause(1)

  val refreshManyTimes = repeat(10) {
    refreshAfterOneSecond
  }
}
